package com.assingment2.access;
import com.assingment2.models.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
public class PostgresDAO {

    private String url;
    private String username;
    private String password;

    public PostgresDAO(
            @Value("${spring.datasource.url}")String url,
            @Value("${spring.datasource.name}")String username,
            @Value("${spring.datasource.password}")String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        testConnection();
    }

    private void testConnection() {
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            System.out.println("Connectedddd.....");
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public Customer getCustomerByID(int id){
        String sql ="SELECT customer_id,first_name,last_name,country,postal_code,phone,email FROM customer where customer_id=?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                        );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }

//    public int insert(Artist artist) {
//        String sql = "INSERT INTO actor VALUES (?,?)";
//        int result = 0;
//        try(Connection conn = DriverManager.getConnection(url, username,password)) {
//            // Write statement
//            PreparedStatement statement = conn.prepareStatement(sql);
//            statement.setInt(1, artist.artist_id());
//            statement.setString(2,artist.name());
//            // Execute statement
//            result = statement.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
}
