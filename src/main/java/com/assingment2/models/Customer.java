package com.assingment2.models;

public record Customer(
        Integer customer_id,
        String first_name,
        String last_name,
        String country,
        String postal_code,
        String phone,
        String email)
{
    @Override
    public Integer customer_id() {
        return customer_id;
    }

    @Override
    public String first_name() {
        return first_name;
    }

    @Override
    public String last_name() {
        return last_name;
    }

    @Override
    public String country() {
        return country;
    }

    @Override
    public String postal_code() {
        return postal_code;
    }

    @Override
    public String phone() {
        return phone;
    }

    @Override
    public String email() {
        return email;
    }
}


