package com.assingment2.repositories;

import java.util.List;

public interface CrudRepository <ID, T>{
    //CRUD
    T getByID(ID id);
    List<T> getAll();
    void create(T object);
    void update(T object);
    void delete(ID id);


}
