package com.assingment2.repositories;


import com.assingment2.models.Customer;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Integer, Customer> {

    public Customer getCustomerByID(int id);
    public List<Customer> getAllCustomers();
    public List<Customer> getAllCustomers(int limit, int offset);
    public List<Customer> getCustomerByName(String name);
//    public int addCustomer(Customer customer);
    public String getCountryWithMostCustomers();

    void addCustomer(Customer customer);
    public Customer getHighestSpenderCustomer();
}


