package com.assingment2.repositories;


import com.assingment2.models.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{


    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}")String url,
            @Value("${spring.datasource.name}")String username,
            @Value("${spring.datasource.password}")String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        //testConnection();
    }
    @Override
    public Customer getByID(Integer integer) {
        return null;
    }

    @Override
    public List<Customer> getAll() {
        return null;
    }

    @Override
    public void create(Customer object) {
    }

    @Override
    public void update(Customer object) {

    }

    @Override
    public void delete(Integer integer) {

    }

    @Override
    public Customer getCustomerByID(int id) {String sql ="SELECT customer_id,first_name,last_name,country,postal_code,phone,email FROM customer where customer_id=?";
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(url,username,password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return customer;
    }

    @Override
    public List<Customer> getAllCustomers() {

            String sql = "SELECT * FROM customer";
            List<Customer> customers = new ArrayList<>();
            try(Connection conn = DriverManager.getConnection(url, username,password)) {
                // Write statement
                PreparedStatement statement = conn.prepareStatement(sql);
                // Execute statement
                ResultSet result = statement.executeQuery();
                // Handle result
                while(result.next()) {
                    Customer customer = new Customer(
                            result.getInt("customer_id"),
                            result.getString("first_name"),
                            result.getString("last_name"),
                            result.getString("country"),
                            result.getString("postal_code"),
                            result.getString("phone"),
                            result.getString("email")
                    );
                    customers.add(customer);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return customers;
        }

    @Override
    public List<Customer> getAllCustomers(int limit,int offset) {
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,limit);
            statement.setInt(2,offset);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public List<Customer> getCustomerByName(String name) {
        String sql = "SELECT * FROM customer WHERE first_name LIKE CONCAT('%', ?, '%')";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,name);
            ResultSet result = statement.executeQuery();

            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;

    }

    @Override
    public String getCountryWithMostCustomers() {
        String sql = "SELECT country,COUNT(country) AS COUNT FROM customer GROUP BY country ORDER BY COUNT DESC LIMIT 1";
        String country = "";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            // Handle result
            ResultSet result = statement.executeQuery();
            while (result.next()){
                country = result.getString("country");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return country;
    }

    @Override
    public void addCustomer(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,customer.first_name());
            statement.setString(2,customer.last_name());
            statement.setString(3,customer.country());
            statement.setString(4,customer.postal_code());
            statement.setString(5,customer.phone());
            statement.setString(6,customer.email());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Customer getHighestSpenderCustomer() {
        String sql = "SELECT customer_id,Sum(Total) as total FROM invoice GROUP BY customer_id ORDER BY total DESC LIMIT 1";
        int maxTotal = 0;
        int customer_idMaxTotal = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute query
            ResultSet result = statement.executeQuery();
            // Handle result
            while (result.next()){
                maxTotal= result.getInt("total");
                customer_idMaxTotal= result.getInt("customer_id");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return getCustomerByID(customer_idMaxTotal);
    }
}

