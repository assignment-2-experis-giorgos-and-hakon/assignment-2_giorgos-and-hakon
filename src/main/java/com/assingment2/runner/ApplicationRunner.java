package com.assingment2.runner;


import com.assingment2.access.PostgresDAO;
import com.assingment2.models.Customer;
import com.assingment2.repositories.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunner implements CommandLineRunner {

    final CustomerRepository customerRepository;
    final PostgresDAO postgresDAO1;

    public ApplicationRunner(CustomerRepository customerRepository, PostgresDAO postgresDAO1) {
        this.customerRepository = customerRepository;
        this.postgresDAO1 = postgresDAO1;
    }


    @Override
    public void run(String... args) throws Exception {
       //System.out.println(postgresDAO1.getCustomerByID(2));
        //System.out.println("it runs..fine");
        //System.out.println(customerRepository.getCustomerByID(2));
       // System.out.println(customerRepository.getAllCustomers());
      // System.out.println(customerRepository.getAllCustomers(3,1));
      //System.out.println(customerRepository.getCountryWithMostCustomers());
      customerRepository.addCustomer(new Customer( 0,"John","Wick","USA","545-555","694201337","JohnWick@mail.com"));

    }
}
